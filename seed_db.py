from datetime import datetime
from hashlib import sha256
import bcrypt

TEST_USER = {
    "email": "admin@admin.com",
    "password": "admin"
}


def clean_db(mongoclient):
    mongoclient.db.users.drop()
    mongoclient.db.files.drop()


def seed(mongoclient):
    user = mongoclient.db.users.insert_one({
        'email': TEST_USER["email"],
        'password': bcrypt.hashpw(TEST_USER["password"].encode('utf-8'), bcrypt.gensalt()),
        'name': '',
        'lastLoginDate': None,
        'createAt': datetime.utcnow(),
        'updateAt': datetime.utcnow(),
    })

    mongoclient.db.files.insert_one({
        'userId': user.inserted_id,
        'originalFileName': "myfile.jpg",
        'fileType': "jpg",
        'filePath': "myfile.jpg",
        'isActive': True,
        'createdAt': datetime.utcnow(),

    })
