import io
import re
from datetime import datetime
from app import create_app
import unittest
from flask_pymongo import PyMongo
from seed_db import clean_db, seed, TEST_USER
from flask import url_for, session


def login(client):
    return client.post(url_for('check_login'), data=dict(email=TEST_USER["email"], password=TEST_USER["password"]),
                       follow_redirects=True)


class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.client = self.app.test_client()
        self.mongo = PyMongo(self.app)
        self.test_file_id = '60b9ddcc5d6aacda558b4a6b'
        clean_db(self.mongo)
        seed(self.mongo)
        return self.app

    def tearDown(self):
        pass

    # Testing without authentication:
    def test_home_redirect(self):
        rv = self.client.get('/')
        assert '302' in rv.status and 'http://localhost/login' in rv.location

    def test_login_status(self):
        rv = self.client.get('/login')
        assert '200' in rv.status

    def test_signup_status(self):
        rv = self.client.get('/signup')
        assert '200' in rv.status

    def test_logout_status(self):
        rv = self.logout()
        assert rv.status_code == 302 and 'http://localhost/login' in rv.location

    def test_share_file_access(self):
        file_link = f'/download/{self.test_file_id}/{self.test_file_id}'
        rv = self.client.get(file_link)
        assert '302' in rv.status and 'http://localhost/login' in rv.location

    def test_download_file_access(self):
        file_link = f'/download_file/{self.test_file_id}'
        rv = self.client.get(file_link)
        assert '302' in rv.status and 'http://localhost/login' in rv.location

    def test_post_check_login(self):
        rv = self.client.post('/check_login')
        assert '302' in rv.status and 'http://localhost/login' in rv.location

    def test_post_handle_signup(self):
        rv = self.client.post('/handle_signup')
        assert '302' in rv.status and 'http://localhost/signup' in rv.location

    def test_post_handle_file_upload(self):
        rv = self.client.post('/handle_file_upload')
        assert '302' in rv.status and 'http://localhost/login' in rv.location

    def test_delete_file_access(self):
        file_link = f'/delete_file/{self.test_file_id}'
        rv = self.client.post(file_link)
        assert '302' in rv.status and 'http://localhost/' in rv.location
        rv = self.client.get(file_link)
        assert '302' in rv.status and 'http://localhost/' in rv.location

    # Authentication testing:
    def signup(self, email=None, password=None):
        return self.client.post('/handle_signup', data=dict(
            email=email,
            password=password,
        ), follow_redirects=True)

    def login(self, email=None, password=None):
        return self.client.post('/check_login', data=dict(
            email=email,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.client.get('/logout')

    def generate_email(self):
        return 'test_user' + str(int(datetime.now().timestamp() * 1000)) + '@gmail.com'

    def test_login_logout(self):
        rv = self.login(TEST_USER['email'], TEST_USER['password'])
        assert rv.status_code == 200 and TEST_USER['email'] in rv.get_data(as_text=True)

        rv = self.logout()
        assert rv.status_code == 302 and 'http://localhost/login' in rv.location

        rv = self.login('some_user_for_test', 'default')
        assert 'No account exists with this email address' in rv.get_data(as_text=True)

        rv = self.login(password='default')
        assert 'Email is required' in rv.get_data(as_text=True)

        rv = self.login(email=TEST_USER['email'])
        assert 'Password is required' in rv.get_data(as_text=True)

        rv = self.login(TEST_USER['email'], 'default')
        assert 'Password is wrong' in rv.get_data(as_text=True)

    def test_signup(self):
        rv = self.signup(TEST_USER['email'], TEST_USER['password'])
        assert 'Email already exists' in rv.get_data(as_text=True)

        rv = self.signup(password='default')
        assert 'Email is required' in rv.get_data(as_text=True)

        rv = self.signup(email='@some_user_for_test.')
        assert 'Password is required' in rv.get_data(as_text=True)

        rv = self.signup('some_user_for_test', 'default')
        assert 'Email is invalid' in rv.get_data(as_text=True)

        rv = self.signup(self.generate_email(), 'default')
        assert 'Your user account is ready.You can login now' in rv.get_data(as_text=True)

    # Testing after authentication:
    def generate_file(self, filecreate=True, filename=''):
        data = dict()
        if filecreate:
            data = {'uploadedFile': (io.BytesIO(b"abcdef"), filename)}
        return data

    def test_upload_files(self):
        with self.app.test_request_context(""):
            session.clear()
            login(self.client)

            data = self.generate_file(filename='test.png')

            rv = self.client.post('/handle_file_upload',
                                  data=data,
                                  follow_redirects=True,
                                  content_type='multipart/form-data'
                                  )
            assert 'You must login again to access this page' not in rv.get_data(as_text=True) and \
                   'No file uploaded ' not in rv.get_data(as_text=True) and \
                   'No selected file' not in rv.get_data(as_text=True) and \
                   'File type not allowed' not in rv.get_data(as_text=True)

            data = self.generate_file()

            rv = self.client.post('/handle_file_upload',
                                  data=data,
                                  follow_redirects=True,
                                  content_type='multipart/form-data'
                                  )
            assert 'No selected file' in rv.get_data(as_text=True)

            data = self.generate_file(filecreate=False)

            rv = self.client.post('/handle_file_upload',
                                  data=data,
                                  follow_redirects=True,
                                  content_type='multipart/form-data'
                                  )
            assert 'No file uploaded' in rv.get_data(as_text=True)

            data = self.generate_file(filename='test.zip')

            rv = self.client.post('/handle_file_upload',
                                  data=data,
                                  follow_redirects=True,
                                  content_type='multipart/form-data'
                                  )
            assert 'File type not allowed' in rv.get_data(as_text=True)

    def test_delete_file(self):
        with self.app.test_request_context(""):
            session.clear()
            login(self.client)
            user = self.mongo.db.users.find_one({"email": TEST_USER["email"]})
            assert_value = self.mongo.db.files.find({"userId": user["_id"]}).count()
            file = self.mongo.db.files.find_one({"userId": user["_id"]})
            self.client.get(url_for('delete_file', fileId=file["_id"]), follow_redirects=True)
            expected_value = self.mongo.db.files.find({"userId": user["_id"]}).count()
            assert expected_value == assert_value - 1

    def test_share_file(self):
        with self.app.test_request_context(""):
            login(self.client)
            data = self.generate_file(filename='test.png')

            rv = self.client.post('/handle_file_upload',
                                  data=data,
                                  follow_redirects=True,
                                  content_type='multipart/form-data'
                                  )
            page_html = rv.get_data(as_text=True)
            assert 'You must login again to access this page' not in page_html and \
                   'No file uploaded ' not in page_html and \
                   'No selected file' not in page_html and \
                   'File type not allowed' not in page_html

            file_id = re.search('data-file-id\="(.+?)"', page_html).group(1)
            file_name = re.search('data-file-name\="(.+?)"', page_html).group(1)
            file_link = f'/download/{file_id}/{file_name}'

            rv = self.client.get(file_link)
            assert rv.status_code == 200 and file_id in rv.get_data(as_text=True)

    def test_download_file(self):
        with self.app.test_request_context(""):
            session.clear()
            login(self.client)
            user = self.mongo.db.users.find_one({"email": TEST_USER["email"]})
            assert_value = self.mongo.db.file_downloads.find({"userId": user["_id"]}).count()
            file = self.mongo.db.files.find_one({"userId": user["_id"]})
            self.client.get(url_for("downloadFile", fileId=file["_id"]), follow_redirects=True)
            expected_value = self.mongo.db.file_downloads.find({"userId": user["_id"]}).count()
            assert expected_value == assert_value + 1

    if __name__ == '__main__':
        unittest.main()
