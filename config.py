class Config:
    MONGO_URI = "mongodb://mongodb:27017/mycloud"
    UPLOAD_FOLDER = "uploads"
    ALLOWED_EXTENSIONS = ['.jpg', '.png', '.jpeg']
    MAX_CONTENT_LENGTH = 3 * 1024 * 1024


class DevelopmentConfig(Config):
    pass


class TestingConfig(Config):
    MONGO_URI = "mongodb+srv://timurdev:Timur_dev_2021@mongoclaster.rdohk.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    #MONGO_URI = "mongodb://mongodb:27017/mycloud_test"


class ProductionConfig(Config):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
