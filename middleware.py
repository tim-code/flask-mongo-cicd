from flask import Flask, Response, request, redirect, session
from functools import wraps

def authenticate(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'userToken' in session:
            if session.get('userToken'):
                return f(*args, **kwargs)
            else:
                session['error'] = 'You must login to access this page'
                return redirect('/login')
        else:
            session.pop('userToken', None)
            session['error'] = 'You must login again to access this page'
            return redirect('/login')
    return wrapper
